<?php

/**
 * @file
 * Main file of Paired Fields.
 */

/**
 * Implements hook_element_info().
 */
function paired_fields_element_info() {
  $types = array();

  $types['paired_fields'] = array(
    '#value' => NULL,
    '#theme_wrappers' => array('paired_fields'),
    '#pre_render' => array('form_pre_render_paired_fields'),
  );

  return $types;
}

/**
 * Implements hook_theme().
 */
function paired_fields_theme() {
  return array(
    'paired_fields' => array(
      'render element' => 'element',
    ),
  );
}

/**
 * Returns HTML for paired fields.
 */
function theme_paired_fields($variables) {
  $element = $variables['element'];
  element_set_attributes($element, array('id'));

  drupal_add_css(drupal_get_path('module', 'paired_fields') . '/paired_fields.css');

  $output = '';

  if (!empty($element['#title'])) {
    $output .= '<label class="paired-field-label">' . $element['#title'] . '</label>';
  }
  if (!empty($element['#description'])) {
    $output .= '<div class="description">' . $element['#description'] . '</div>';
  }
  if (isset($element['#pairs'])) {
    foreach ($element['#pairs'] as $key => $value) {
      $output .= '<div id="' . $element['#id'] . '" class="paired-fields">';
      foreach ($value as $pair_member_key => $pair_member) {
        $output .= '<div class="pair-member">' . $pair_member . '</div>';
      }
      $output .= '</div>';
    }
  }
  return $output;
}

/**
 * Adds members of this group as actual elements for rendering.
 */
function form_pre_render_paired_fields($element) {
  $pair_name = 0;
  $pair_names = array();

  $disallowed_types = array('value', 'hidden', 'actions');

  $i = 1;
  foreach (element_children($element) as $key) {
    if  (isset($element[$key]['#type']) && !in_array($element[$key]['#type'], $disallowed_types)) {
      if ($i % 2) {
        $left = drupal_render($element[$key]);
        $left_name = $key;
      }
      else {
        $element['#pairs'][$left_name . ' & ' . $key] = array(
          $left_name => $left,
          $key => drupal_render($element[$key]),
        );
      }
      $i++;
    }
    else {
      if (!isset($element['#suffix'])) {
        $element['#suffix'] = '';
      }
      $element['#suffix'] .= drupal_render($element[$key]);
    }
  }
  return $element;
}

/**
 * Implements hook_webform_component_info().
 */
function paired_fields_webform_component_info() {
  $component_info = array(
    'paired_fields' => array(
      'label' => t('Paired fields'),
      'description' => t('Paired fields allows you to arrange multiple fields side-by-side.'),
      'features' => array(
        'csv' => FALSE,
        'default_value' => FALSE,
        'required' => FALSE,
        'conditional' => FALSE,
        'group' => TRUE,
        'title_inline' => FALSE,
      ),
      'file' => 'webform.inc',
    ),
  );

  return $component_info;
}
